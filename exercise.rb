class Exercise

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
  
    # Declare our result
    result = []
    
    # Tokenize our string
    tokens = str.split
    
    # Go through each word of tokenized string
    tokens.each do |token|
        
        # Declare our punctuation to maintain
        punctuation = nil
        
        # Check if we're exceeding 4 characters
        if !token[4].nil?
          
          # Check for punctuation to maintain
          if !/[0-9a-zA-Z]/.match(token[token.length - 1])
            #puts "Got some punctuation"
            punctuation = token[token.length - 1]
          end
          
          # Check for a capital first letter
          if token[0] == token[0].capitalize
            token = "Marklar"
          else
            token = "marklar"
          end
        end
        
        # Add back any punctuation if needed
        if !punctuation.nil?
          token << punctuation
        end
        
        # Append to our result array
        result << token
        
      end
     
    # Make resultant string and return it
    return result.join(" ")
    
  end

  # Since this is a common programming problem, I've borrowed the implementation from:
  # http://stackoverflow.com/questions/12178642/fibonacci-sequence-in-ruby-recursion
  def self.fibonacci(n)
    n <= 1 ? n :  fibonacci( n - 1 ) + fibonacci( n - 2 ) 
  end
  
  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.even_fibonacci(nth)
  
    evenTerms = []
    termIndex = 0;
    sum = 0;
    
    # Compute the fibonacci sequence term by term
    while termIndex <= nth
    
      term = self.fibonacci(termIndex)
      
      # Add term to our list if it's even
      if term % 2 == 0
        evenTerms << term
      end
    
      termIndex += 1
      
    end
    
    # Sum the even terms
    evenTerms.each do |evenTerm|
      sum += evenTerm
    end
    
    return sum
    
  end

end
